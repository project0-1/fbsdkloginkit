
Pod::Spec.new do |s|

  s.name         = "FBSDKLoginKit"
  s.version      = "4.4.0"
  s.summary      = "FBSDKLoginKit framework."

  s.description  = <<-DESC
                   FBSDKLoginKit framework.

                   DESC

  s.homepage     = "https://developers.facebook.com/docs/ios"

s.platform     = :ios, "8.0"

  s.source       = { :git => "git@bitbucket.org:project0-1/fbsdkloginkit.git" }


  s.source_files  = "*.*", "Headers/*.{h,m,swift}", "Modules/*.modulemap"



end
